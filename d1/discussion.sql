[1] SECTION Add new records

-- 5 new artists

INSERT INTO artists (name) VALUES
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

-- 2 albums for Taylor

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Fearless", "2008-1-1", 3),
("Red", "2012-1-1", 3);

-- 2 songs for fearless album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Fearless", 246, "Pop rock", 7),
("State of Grace", 213, "Country Pop", 7);

-- 2 songs for red album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Love Story", 312, "Country Rock", 8),
("Begin Again", 304, "Country", 8);

-- 2 albums for Lady G.

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("A Star is Born", "2018-1-1", 4),
("Born This Way", "2011-1-1", 4);

-- 2 songs for A Star is Born album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Shallow", 256, "Country, Rock, Pop Rock", 9),
("Always Remember Us This Way", 310, "Country", 9);

-- 2 songs for Born This Way album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Bad Romance", 320, "Electro, Pop", 10),
("Paparazzi", 258, "Pop", 10);

-- 2 albums for Justin B.

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Purpose", "2015-1-1", 5),
("Believe", "2012-1-1", 5);

-- 1 songs for Purpose album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Sorry", 242, "Dancehall-poptropical", 11);

-- 1 songs for Believe album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Boyfriend", 320, "Pop", 12);

-- 2 albums for Ariana G.

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Dangerous Woman", "2016-1-1", 6),
("Thank U, Next", "2019-1-1", 6);

-- 1 songs for Dangerous Woman album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Into You", 320, "EDM house", 13);

-- 1 songs for Thank U, Next album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Thank U, Next", 246, "Pop, R&B", 14);

-- 2 albums for Bruno M.

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("24K Magic", "2016-1-1", 7),
("Earth to Mars", "2011-1-1", 7);

-- 1 songs for 24K Magic album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("24K Magic", 400, "Funk, Disco, R&B", 15);

-- 1 songs for Earth to Mars album

INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Lost", 250, "Pop", 16);


[2] SECTION - Advanced Selection

SELECT * FROM songs WHERE id = 11;
SELECT * FROM songs WHERE id != 11;
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id >= 11;


-- GET specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- GET specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

-- Combining conditions
SELECT * FROM songs WHERE album_id = 5 AND id < 8;

-- Find Partial Matches
SELECT * FROM songs WHERE song_name LIKE "%a"; -- select keyword from the end.

SELECT * FROM albums WHERE date_released LIKE "201_-01-01";

-- Sorting Keywords
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct records
SELECT DISTINCT genre FROM songs;
SELECT DISTINCT song_name FROM songs;

[3] Table Joins
-- Combine artist table and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included in a table
SELECT artists.name, albums.album_title, songs.song_name FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;